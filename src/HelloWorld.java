import java.util.Scanner;

public class HelloWorld {
    static void order(String food){
        System.out.println("You have ordered "+ food + ". " +"Thank you!");
    }

    public static void main(String[] args) {
        System.out.println("What would you like to order:");
        System.out.println("1. Tempura");
        System.out.println("2. Ramen");
        System.out.println("3. Udon");
        System.out.println("Your order [1-3]:");
        Scanner userIn = new Scanner(System.in);
        int number= userIn.nextInt();
        userIn.close();

        switch (number) {
            case 1:
                order("Tempura");
                break;
            case 2:
                order("Ramen");
                break;
            case 3:
                order("Udon");
                break;
            default:
                System.out.println("error");
        }
    }
}
